/* Controllers */
const rfidNombreSelecInventarioController = require('../controllers/rfid_nombreSelecInventario');
const rfidNombreArchivoCsvController = require('../controllers/rfid_nombreArchivoCsv');
const sucursalsController = require('../controllers/sucursals');
const rfidController = require('../controllers/rfid');


module.exports = (app) => {
   app.get('/api', (req, res) => res.status(200).send ({
        message: 'Example project did not give you access to the api web services',
   }));

   app.get('/api/rfidNombreSelecInventario', rfidNombreSelecInventarioController.findAll);
   app.post('/api/rfidNombreSelecInventario', rfidNombreSelecInventarioController.create);

   app.get('/api/rfidNombreArchivoCsv', rfidNombreArchivoCsvController.findAll);
   app.post('/api/rfidNombreArchivoCsv', rfidNombreArchivoCsvController.create);

   app.get('/api/sucursals', sucursalsController.findAll);


   app.post('/api/rfid', rfidController.create);
   app.get('/api/rfid/:id', rfidController.find);

   // app.patch('/api/mascota/:id', petController.update);
   // app.get('/api/mascota', petController.findAll);
   // app.get('/api/mascota/:id', petController.find);
   // app.delete('/api/mascota/:id', petController.delete);

   // app.post('/api/usuario', userController.create);
   // app.post('/api/usuario/login', userController.login);
   // app.get('/api/usuario/list', userController.list);
   // app.get('/api/usuario/:id', userController.find);
   // app.delete('/api/usuario/:id', userController.delete);
   // app.patch('/api/usuario/:id', userController.update);

};
