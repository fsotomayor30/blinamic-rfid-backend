const sql = require('mssql');

module.exports = {

    async create(request, res) {
        var body = request.body;
        var date = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
        try{
            await sql.connect('mssql://rosario:Hasar1102@192.168.100.60/TodoBikesOP')
            var query = `insert into Rfid_NombreArchivoCsv(fechahora, Sucursal_ID, nombre) values ('${date}', ${request.body.sucursalID}, '${request.body.fileName}'); SELECT SCOPE_IDENTITY() AS id;`;
            var result = await sql.query(query);

            for(element of body.correlatives){

                var query = `select * from Rfid_ProductosCorrelativos where Correlativo = ${element}`;
                var result2 = await sql.query(query);

                if(result2['recordset'].lenght > 0){
                    var query = `insert into Rfid_DatosArchivoCsv(Correlativo, ProductosID, Rfid_NombreArchivoCsvID, CodigoP, Sucursal_ID) values (${result2['recordset'][0]['Correlativo']}, ${result2['recordset'][0]['ProductosID']}, ${result['recordset'][0]['id']}, ${result2['recordset'][0]['CodigoP']}, ${body.sucursalID})`;
                    var result3 = await sql.query(query);
                }else{
                    var query = `insert into Rfid_DatosArchivoCsv(Correlativo, ProductosID, Rfid_NombreArchivoCsvID, CodigoP, Sucursal_ID) values (${element}, NULL, ${result['recordset'][0]['id']}, NULL, ${body.sucursalID})`;
                    var result3 = await sql.query(query);
                }


            }

            var query = `insert into Rfid_CsvXInventarioSelec(FechaCreacion, Rfid_NombreArchivoCsv_ID, Rfid_NombreSelecInventario_ID) values ('${date}', ${result['recordset'][0]['id']},  ${body.selectInventarioID})`;
            var result4 = await sql.query(query);
            
            res.status(201).send();

        }catch (e){
            console.log(e.message);
            res.status(500).send(e.message);
        }
    },
     find (req, res) {
        async function findAll  ()  {
            await sql.connect('mssql://rosario:Hasar1102@192.168.100.60/TodoBikesOP')
            const result = await sql.query`select rfidnsi.Nombre as NombreSelec, rfidnac.Nombre as NombreArchivo from Rfid_CsvXInventarioSelec rfidcxi inner join Rfid_NombreArchivoCsv rfidnac on rfidcxi.Rfid_NombreArchivoCsv_ID = rfidnac.ID inner join Rfid_NombreSelecInventario rfidnsi on rfidcxi.Rfid_NombreSelecInventario_ID = rfidnsi.ID where rfidnsi.ID=${req.params.id}`
            return result;
     }

     findAll().then(val => res.send(val['recordsets'][0]));
    }
 };
