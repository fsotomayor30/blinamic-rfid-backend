const sql = require('mssql');

module.exports = {
    findAll(_, res) {
        async function findAll  ()  {
            await sql.connect('mssql://rosario:Hasar1102@192.168.100.60/TodoBikesOP')
            const result = await sql.query`select * from Rfid_NombreSelecInventario`
            return result;
     }

     findAll().then(val => res.send(val['recordsets'][0]));
    },
    create(request, res) {
        async function insert  ()  {
            await sql.connect('mssql://rosario:Hasar1102@192.168.100.60/TodoBikesOP')
            var query = `insert into Rfid_DatosArchivoCsv(Correlativo, ProductosID, Rfid_NombreArchivoCsvID, CodigoP) values (${request.body.correlativo}, ${request.body.productosID}, ${request.body.rfidNombreArchivoCsvID}, ${request.body.codigoP})`;

            const result = await sql.query(query);
            return result;
        }  

    insert().then(val => res.status(201).send(val));
    }
 };
